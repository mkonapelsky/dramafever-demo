//
//  Utilities.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/1/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class Utilities: NSObject {

    
    /**
     Create a basic alert used to convey information.
     
     @param title The title that should be displayed on the alert
     @param message The message to display.
     @param style The style to use for the alert. (.actionSheet, .alert)
     
     @return A ready to use UIActionController.
     */
    class func ShowBasicAlert(title: String, message: String, style: UIAlertControllerStyle) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: style) // .alert
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        
        return alert
    }
}
