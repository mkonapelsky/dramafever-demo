//
//  Company.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/1/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class Company: NSObject {

    var name: String?
    var catchPhrase: String?
    var bs: String?
    
    override init() { }
    
    init(name: String, catchPhrase: String, bs: String) {
        
        self.name = name
        self.catchPhrase = catchPhrase
        self.bs = bs
    }
    
    class func buildWithJSON(json: Dictionary<String, AnyObject>) -> Company {
        
        let company = Company()
        
        company.name = json["name"] as? String
        company.catchPhrase = json["catchPhrase"] as? String
        company.bs = json["bs"] as? String
        
        return company
    }
}
