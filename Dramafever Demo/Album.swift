//
//  Album.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/1/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class Album: NSObject {

    var id: Int?
    var userId: Int?
    var title: String?
    var photos: [Photo] = [Photo]()
    
    override init() { }
    
    init(id: Int, userId: Int, title: String) {
        
        self.id = id
        self.userId = userId
        self.title = title
    }
    
    class func buildWithJSON(json: Dictionary<String, AnyObject>) -> Album {
        
        let album = Album()
        
        album.id = json["id"] as? Int
        album.userId = json["userId"] as? Int
        album.title = json["title"] as? String
        
        return album
    }
    
    func getPhotos(completion: @escaping ([Photo]?, String?) -> Void) {
        
        let urlSession: URLSession = URLSession.shared
        
        let dataTask = urlSession.dataTask(with: URL(string: "https://jsonplaceholder.typicode.com/albums/\(self.id!)/photos")!)  {
            data, response, error in
            
            if error != nil {
                completion(nil, error!.localizedDescription)
            }
            else if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200 {
                    
                    do {
                        
                        let photosJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [Dictionary<String, AnyObject>]
                        
                        var photos = [Photo]()
                        for photo in photosJSON {
                            photos.append(Photo.buildWithJSON(json: photo))
                        }
                        
                        completion(photos, nil)
                    }
                    catch {
                        completion(nil, "Unable to get photos for album.")
                    }
                }
                else {
                    completion(nil, "Unable to get photos for album.")
                }
            }
        }
        
        dataTask.resume()
    }
    
    func getImages(completion: @escaping ([UIImage]?, String?) -> Void) {
        
        var images = [UIImage]()
        
        DispatchQueue.global(qos: .background).async {
            
            for photo in self.photos {
                
                let photourl = URL(string: photo.url!)
                
                let task = URLSession.shared.dataTask(with: photourl!) {
                    data, response, error in
                    
                    if error != nil {
                        
                    }
                    else {
                        
                        images.append(UIImage(data: data!)!)
                    }
                }
                
                task.resume()
                
            }
            
            DispatchQueue.main.async {
                completion(images, nil)
            }
        }
    }
}
