//
//  Address.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/1/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class Address: NSObject {

    var street: String?
    var suite: String?
    var city: String?
    var zipCode: String?
    var geo: Geo?
    
    override init() { }
    
    init(street: String, suite: String, city: String, zipCode: String, geo: Geo) {
        
        self.street = street
        self.suite = suite
        self.city = city
        self.zipCode = zipCode
        self.geo = geo
    }
    
    class func buildWithJSON(json: Dictionary<String, AnyObject>) -> Address {
        
        let address = Address()
        
        address.street = json["street"] as? String
        address.suite = json["suite"] as? String
        address.city = json["city"] as? String
        address.zipCode = json["zipcode"] as? String
        address.geo = Geo.buildWithJSON(json: json["geo"] as! Dictionary<String, AnyObject>)
        
        return address
    }
}
