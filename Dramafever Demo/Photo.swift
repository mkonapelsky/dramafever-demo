//
//  Photo.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/1/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class Photo: NSObject {
    
    var id: Int?
    var albumId: Int?
    var title: String?
    var url: String?
    var thumbnailUrl: String?
    
    override init() { }
    
    init(id: Int, albumId: Int, title: String, url: String, thumbnailUrl: String) {
        
        self.id = id
        self.albumId = albumId
        self.title = title
        self.url = url
        self.thumbnailUrl = thumbnailUrl
    }
    
    class func buildWithJSON(json: Dictionary<String, AnyObject>) -> Photo {
        
        let photo = Photo()
        
        photo.id = json["id"] as? Int
        photo.albumId = json["albumId"] as? Int
        photo.title = json["title"] as? String
        photo.url = json["url"] as? String
        photo.thumbnailUrl = json["thumbnailUrl"] as? String
        
        return photo
    }
    
    func getImage(completion: @escaping(UIImage?, String?) -> Void) {
        
        DispatchQueue.global(qos: .background).async {
            
            let photourl = URL(string: self.url!)
            
            let task = URLSession.shared.dataTask(with: photourl!) {
                data, response, error in
                
                if error != nil {
                    completion(nil, error!.localizedDescription)
                }
                else {
                    
                    let image = UIImage(data: data!)
                    
                    DispatchQueue.main.async {
                        completion(image, nil)
                    }
                }
            }
            
            task.resume()
        }
    }
}
