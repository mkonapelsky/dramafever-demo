//
//  Geo.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/1/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class Geo: NSObject {

    var lat: String?
    var lng: String?
    
    override init() { }
    
    init(lat: String, lng: String) {
        
        self.lat = lat
        self.lng = lng
    }
    
    class func buildWithJSON(json: Dictionary<String, AnyObject>) -> Geo {
        
        let geo = Geo()
        
        geo.lat = json["lat"] as? String
        geo.lng = json["lng"] as? String
        
        return geo
    }
}
