//
//  Post.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/1/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class Post: NSObject {
    
    var id: Int?
    var userId: Int?
    var title: String?
    var body: String?
    var comments: [Comment] = [Comment]()
    
    override init() { }
    
    init(id: Int, userId: Int, title: String, body: String) {
        
        self.id = id
        self.userId = userId
        self.title = title
        self.body = body
    }
    
    class func buildWithJSON(json: Dictionary<String, AnyObject>) -> Post {
        
        let post = Post()
        
        post.id = json["id"] as? Int
        post.userId = json["userId"] as? Int
        post.title = json["title"] as? String
        post.body = json["body"] as? String
        post.getComments() {
            postComments, errorMessage in
            
            // TODO: Implement error handling
            for c in postComments!
            {
                post.comments.append(c)
            }
        }
        
        return post
    }
    
    func getComments(completion: @escaping ([Comment]?, String?) -> Void) {
        
        let urlSession: URLSession = URLSession.shared
        
        let dataTask = urlSession.dataTask(with: URL(string: "https://jsonplaceholder.typicode.com/posts/\(self.id!)/comments")!) {
            data, response, error in
            
            
            if error != nil {
                completion(nil, error?.localizedDescription)
            }
            else if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200 {
                    
                    do {
                        
                        let commentsJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [Dictionary<String, AnyObject>]
                        
                        var postComments = [Comment]()
                        for c in commentsJSON {
                            postComments.append(Comment.buildWithJSON(json: c))
                        }
                        
                        completion(postComments, nil)
                    }
                    catch {
                        completion(nil, "Unable to get comments for post.")
                    }
                }
                else {
                    
                    completion(nil, "Unable to get comments for post.")
                }
            }
        }
        
        dataTask.resume()
    }
}
