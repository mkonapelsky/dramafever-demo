//
//  User.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/1/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class User: NSObject {
    
    var id: Int?
    var name: String?
    var userName: String?
    var email: String?
    var phone: String?
    var website: String?
    var address: Address?
    var company: Company?
    var posts: [Post] = [Post]()
    var albums: [Album] = [Album]()

    override init() { }
    
    init(id: Int, name: String, userName: String, email: String, phone: String, website: String, address: Address, company: Company) {
        
        self.id = id
        self.name = name
        self.userName = userName
        self.email = email
        self.phone = phone
        self.website = website
        self.address = address
        self.company = company
    }
    
    class func BuildWithJSON(json: Dictionary<String, AnyObject>) -> User {
        
        let user = User()
        
        user.id = json["id"] as? Int
        user.name = json["name"] as? String
        user.userName = json["username"] as? String
        user.email = json["email"] as? String
        user.phone = json["phone"] as? String
        user.website = json["website"] as? String
        user.address = Address.buildWithJSON(json: json["address"] as! Dictionary<String, AnyObject>)
        user.company = Company.buildWithJSON(json: json["company"] as! Dictionary<String, AnyObject>)
        
        return user
    }
    
    func getAlbums(completion: @escaping ([Album]?, String?) -> Void) {
        
        let urlSession: URLSession = URLSession.shared
        
        let dataTask = urlSession.dataTask(with: URL(string: "https://jsonplaceholder.typicode.com/users/\(self.id!)/albums")!) {
            data, response, error in
            
            
            DispatchQueue.main.async {
                if error != nil {
                    completion(nil, error?.localizedDescription)
                }
                else if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode == 200 {
                        
                        do {
                            
                            let albumsJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [Dictionary<String, AnyObject>]
                            
                            var userAlbums = [Album]()
                            for a in albumsJSON {
                                userAlbums.append(Album.buildWithJSON(json: a))
                            }
                            
                            completion(userAlbums, nil)
                        }
                        catch {
                            completion(nil, "Unable to get albums for user.")
                        }
                    }
                    else {
                        
                        completion(nil, "Unable to get albums for user.")
                    }
                }
            }
        }
        
        dataTask.resume()
    }
    
    func getPosts(completion: @escaping ([Post]?, String?) -> Void) {
        
        let urlSession: URLSession = URLSession.shared
        
        let dataTask = urlSession.dataTask(with: URL(string: "https://jsonplaceholder.typicode.com/users/\(self.id!)/posts")!) {
            data, response, error in
            
            
            DispatchQueue.main.async {
                if error != nil {
                    completion(nil, error?.localizedDescription)
                }
                else if let httpResponse = response as? HTTPURLResponse
                {
                    if httpResponse.statusCode == 200 {
                        
                        do {
                            
                            let postsJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [Dictionary<String, AnyObject>]
                            
                            var userPosts = [Post]()
                            for p in postsJSON {
                                userPosts.append(Post.buildWithJSON(json: p))
                            }
                            
                            completion(userPosts, nil)
                        }
                        catch {
                            completion(nil, "Unable to get posts for user.")
                        }
                    }
                    else {
                        
                        completion(nil, "Unable to get posts for user.")
                    }
                }
            }
        }
        
        dataTask.resume()
    }
    
    class func GetAllUsers(completion: @escaping ([User]?, String?) -> Void) {
        
        var users = [User]()
        let urlSession: URLSession = URLSession.shared
        
        let dataTask = urlSession.dataTask(with: URL(string: "https://jsonplaceholder.typicode.com/users")!) {
            data, response, error in
            
            
            if error != nil {
                
                completion(nil, error!.localizedDescription)
            }
            else if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode == 200 {
                    
                    do {
                        
                        let userJSON = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [Dictionary<String, AnyObject>]
                        
                        for u in userJSON {
                            users.append(User.BuildWithJSON(json: u))
                        }
                        
                        completion(users, nil)
                    }
                    catch {
                        completion(nil, "Unable to retrieve list of users. Status code \(httpResponse.statusCode).")
                    }
                }
                else {
                    
                    completion(nil, "Unable to retrieve list of users. Status code \(httpResponse.statusCode).")
                }
            }
        }
        
        dataTask.resume()
    }
}
