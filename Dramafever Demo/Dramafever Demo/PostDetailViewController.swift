//
//  PostDetailViewController.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/4/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class PostDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var post: Post!
    @IBOutlet private var tblPost: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.tblPost.delegate = self
        self.tblPost.dataSource = self
        
        self.tblPost.rowHeight = UITableViewAutomaticDimension
        self.tblPost.estimatedRowHeight = 140
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Table
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44.0))
            view.backgroundColor = UIColor(red:0.06, green:0.51, blue:0.96, alpha:1.0)
            let label = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.size.width, height: 44.0))
            label.textColor = UIColor.white
            label.text = "Post"
            view.addSubview(label)
            
            return view
        }
        else {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44.0))
            view.backgroundColor = UIColor(red:0.06, green:0.51, blue:0.96, alpha:1.0)
            let label = UILabel(frame: CGRect(x: 10, y: 0, width: view.frame.size.width, height: 44.0))
            label.textColor = UIColor.white
            label.text = "Comments"
            view.addSubview(label)
            
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return self.post.comments.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            var cell = tableView.dequeueReusableCell(withIdentifier: "postCell") as? UserPostTableViewCell
            if cell == nil {
                cell = UserPostTableViewCell(style: .default, reuseIdentifier: "postCell")
            }
            
            // Fill out cell
            cell?.lblPost.text = self.post.body
            
            return cell!
        }
        else {
            var cell = tableView.dequeueReusableCell(withIdentifier: "commentCell") as? UserCommentTableViewCell
            if cell == nil {
                cell = UserCommentTableViewCell(style: .default, reuseIdentifier: "commentCell")
            }
            
            let comment = self.post.comments[indexPath.row]
            
            // Fill out cell
            cell?.body.text = comment.body
            cell?.lblNameAndEmail.text = "- \(comment.email!)"
            
            return cell!
        }
    }
}
