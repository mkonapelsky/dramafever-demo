//
//  PresentImageViewController.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/6/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class PresentImageViewController: UIViewController {

    @IBOutlet private var imageView: UIImageView!
    var image: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.imageView.image = self.image
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
