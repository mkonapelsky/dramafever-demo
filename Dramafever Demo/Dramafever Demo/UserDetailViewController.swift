//
//  UserDetailViewController.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/3/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit
import MapKit
import MessageUI

class UserDetailViewController: UIViewController, MFMailComposeViewControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    var user: User!
    @IBOutlet private var mkGeo: MKMapView!
    @IBOutlet private var lblAddress: UILabel!
    @IBOutlet private var lblCSZ: UILabel!
    @IBOutlet private var btnCall: UIButton!
    @IBOutlet private var btnEMail: UIButton!
    @IBOutlet private var btnWeb: UIButton!
    @IBOutlet private var tblWork: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "@\(user!.userName!)"
        self.tblWork.delegate = self
        self.tblWork.dataSource = self
        
        // Map - This api seems to use bogus coordinates.
        let mapCenter = CLLocationCoordinate2DMake(Double(self.user!.address!.geo!.lat!)!, Double(self.user!.address!.geo!.lng!)!)
        let span = MKCoordinateSpanMake(100, 100)
        let region = MKCoordinateRegionMake(mapCenter, span)
        self.mkGeo.region = region
        
        let annotation = MKPointAnnotation()
        let centerCoordinate = mapCenter
        annotation.coordinate = centerCoordinate
        annotation.title = "Title"
        mkGeo.addAnnotation(annotation)
        
        // Labels
        self.lblAddress.text = user?.address?.street
        self.lblCSZ.text = "\(user!.address!.city!) \(user!.address!.zipCode!)"
        
        // Style buttons
        self.btnCall.backgroundColor = UIColor(red:0.98, green:0.75, blue:0.51, alpha:1.0)
        self.btnEMail.backgroundColor = UIColor(red:0.98, green:0.75, blue:0.51, alpha:1.0)
        self.btnWeb.backgroundColor = UIColor(red:0.98, green:0.75, blue:0.51, alpha:1.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func call() {
        let phoneNumber = URL(string: "tel://\(user!.phone!)")
        
        if UIApplication.shared.canOpenURL(phoneNumber!) {
            UIApplication.shared.open(phoneNumber!, options: [:], completionHandler: nil)
        }
        else {
            self.present(Utilities.ShowBasicAlert(title: "Oops", message: "Unable to call \(self.user!.name!)", style: .alert), animated: true)
        }
    }
    
    @IBAction func email() {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["\(self.user!.email!)"])
            mail.setSubject("Query from Art Connect")
            
            present(mail, animated: true)
        }
        else {
            self.present(Utilities.ShowBasicAlert(title: "Oops", message: "Unable to email \(self.user!.name!)", style: .alert), animated: true)

        }
    }
    
    @IBAction func web() {
        let webSite = URL(string: "http://\(user!.website!)")
        
        if UIApplication.shared.canOpenURL(webSite!) {
            UIApplication.shared.open(webSite!, options: [:], completionHandler: nil)
        }
        else {
            self.present(Utilities.ShowBasicAlert(title: "Oops", message: "Unable to find a website for \(self.user!.name!)", style: .alert), animated: true)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    // Table
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        
        cell?.accessoryType = .disclosureIndicator
        
        if indexPath.row == 0 {
            cell?.textLabel?.text = "\(self.user.posts.count) Posts"
        }
        else {
            cell?.textLabel?.text = "\(self.user.albums.count) Albums"
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if indexPath.row == 0 {
            let postsView = storyboard.instantiateViewController(withIdentifier: "postsView") as! UserPostsViewController
            postsView.user = self.user
            self.navigationController?.pushViewController(postsView, animated: true)
        }
        else {
            let albumsView = storyboard.instantiateViewController(withIdentifier: "albumsView") as! UserAlbumsViewController
            albumsView.user = self.user
            self.navigationController?.pushViewController(albumsView, animated: true)
        }
    }
}
