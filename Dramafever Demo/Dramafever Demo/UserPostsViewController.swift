//
//  UserPostsViewController.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/4/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class UserPostsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var user: User!
    @IBOutlet private var tblPosts: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Posts"
        
        self.tblPosts.delegate = self
        self.tblPosts.dataSource = self
        
        self.tblPosts.rowHeight = UITableViewAutomaticDimension
        self.tblPosts.estimatedRowHeight = 74.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.user.posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "postListCell") as? PostListTableViewCell
        if cell == nil {
            cell = PostListTableViewCell(style: .subtitle, reuseIdentifier: "postListCell")
        }
        
        cell?.isUserInteractionEnabled = false
        
        let post = self.user.posts[indexPath.row]
        
        // Load comments
        DispatchQueue.global(qos: .background).async {
            
            post.getComments() {
                postComments, errorMessage in
                
                DispatchQueue.main.async {
                    
                    cell?.lblTitle.text = post.title!
                    cell?.lblCommentCount?.text = "\(postComments!.count) Comments"
                    cell?.isUserInteractionEnabled = true
                }
            }
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyboard.instantiateViewController(withIdentifier: "postDetailView") as! PostDetailViewController
        view.post = self.user.posts[indexPath.row]
        
        self.navigationController?.pushViewController(view, animated: true)
    }
}
