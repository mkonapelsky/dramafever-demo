//
//  ViewController.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/1/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit
import MapKit

class UserSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {

    @IBOutlet private var userTable: UITableView!
    private var users = [User]()
    private var filteredUsers = [User]()
    private let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        self.userTable.tableHeaderView = searchController.searchBar
        self.userTable.delegate = self
        self.userTable.dataSource = self
        
        self.searchController.searchBar.tintColor = UIColor.white
        self.searchController.searchBar.barTintColor = UIColor(red:0.06, green:0.51, blue:0.96, alpha:1.0)
        
        self.title = "Art Connect"
        SVProgressHUD.show(withStatus: "Getting users ...")
        
        DispatchQueue.global(qos: .background).async {
            
            User.GetAllUsers() {
                usersCollection, error in
                
                DispatchQueue.main.async {
                    
                    if error != nil {
                        SVProgressHUD.dismiss()
                        self.present(Utilities.ShowBasicAlert(title: "error", message: error!, style: .alert), animated: true)
                    }
                    else {
                        self.users = usersCollection!
                        self.userTable.reloadData()
                        
                        SVProgressHUD.dismiss()
                    }
                }
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // UITableView Delegate / DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return self.filteredUsers.count
        }
        return self.users.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "userCell") as? UserTableViewCell
        if cell == nil
        {
            cell = UITableViewCell(style: .default, reuseIdentifier: "userCell") as? UserTableViewCell
        }
        
        var user: User
        
        if searchController.isActive && searchController.searchBar.text != "" {
            user = self.filteredUsers[indexPath.row]
        } else {
            user = self.users[indexPath.row]
        }
        
        DispatchQueue.global(qos: .background).async {
            
            user.getAlbums() {
                userAlbums, errorMessage in
                
                if errorMessage == nil {
                    
                    user.albums = userAlbums!
                    cell?.spinnerAlbums.stopAnimating()
                    cell?.lblAlbumsCount.isHidden = false
                    cell?.lblAlbumsCount.text = "\(user.albums.count) Albums"
                }
            }
            
            user.getPosts() {
                userPosts, errorMessage in
                
                if errorMessage == nil {
                    
                    user.posts = userPosts!
                    cell?.spinnerPosts.stopAnimating()
                    cell?.lblPostsCount.isHidden = false
                    cell?.lblPostsCount.text = "\(user.posts.count) Posts"
                }
            }
        }
        
        
        cell?.userImage.image = UIImage(named: "User")
        cell?.lblUser.text = user.name
        cell?.lblUserName.text = "@\(user.userName!)"
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = self.users[indexPath.row]
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let userDetailView = storyBoard.instantiateViewController(withIdentifier: "userDetailView") as! UserDetailViewController
        userDetailView.user = user
        
        self.navigationController?.pushViewController(userDetailView, animated: true)
        
    }
    
    // UISearchUpdating  + Helper
    func filterContentForSearch(text: String) {
        
        filteredUsers = self.users.filter {
            user in
            return user.name!.lowercased().contains(text.lowercased()) ||
                   user.userName!.lowercased().contains(text.lowercased())
        }
        
        self.userTable.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearch(text: searchController.searchBar.text!)
    }
}

