//
//  PhotoDetailCollectionViewCell.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/5/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class PhotoDetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
