//
//  UserPostTableViewCell.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/4/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class UserPostTableViewCell: UITableViewCell {

    @IBOutlet var lblPost: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
