//
//  UserTableViewCell.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/2/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit
import MapKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet var userImage: UIImageView!
    @IBOutlet var lblUser: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblPostsCount: UILabel!
    @IBOutlet var lblAlbumsCount: UILabel!
    @IBOutlet var spinnerPosts: UIActivityIndicatorView!
    @IBOutlet var spinnerAlbums: UIActivityIndicatorView!
    //@IBOutlet var vwPosts: UIView!
    //@IBOutlet var vwAlbums: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        spinnerPosts.startAnimating()
        spinnerAlbums.startAnimating()
        
        lblPostsCount.isHidden = true
        lblAlbumsCount.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
