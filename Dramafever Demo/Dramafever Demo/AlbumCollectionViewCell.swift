//
//  AlbumCollectionViewCell.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/4/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var activityLoading: UIActivityIndicatorView!
    @IBOutlet var imagePreview: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
