//
//  Comment.swift
//  Dramafever Demo
//
//  Created by Michael Konapelsky on 8/1/17.
//  Copyright © 2017 Michael Konapelsky. All rights reserved.
//

import UIKit

class Comment: NSObject {
    
    var id: Int?
    var postId: Int?
    var name: String?
    var email: String?
    var body: String?

    override init() { }
    
    init(id: Int, postId: Int, name: String, email: String, body: String) {
        
        self.id = id
        self.postId = postId
        self.name = name
        self.email = email
        self.body = body
    }
    
    class func buildWithJSON(json: Dictionary<String, AnyObject>) -> Comment {
        
        let comment = Comment()
        
        comment.id = json["id"] as? Int
        comment.postId = json["postId"] as? Int
        comment.name = json["name"] as? String
        comment.email = json["email"] as? String
        comment.body = json["body"] as? String
        
        return comment
    }
}
